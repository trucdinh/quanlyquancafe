

--Food
--Table
--FoodCategory
--Account
--Bill
--BillInfo

create table TableFood(
	id int identity primary key,
	name nvarchar(100) not null,
	status nvarchar(100) not null default N'Trống' --Trống or Có người 
)
go

create table Account(
	id int identity primary key,
	UserName nvarchar(100) not null,
	DisplayName nvarchar(100) not null,
	PassWord nvarchar(100) not null,
	Type int not null default 0 -- 1:Admin or 0:Staff
)
go

create table FoodCategory(
	id int identity primary key,
	name nvarchar(100) not null,
)
go

create table Food(
	id int identity primary key,
	name nvarchar(100) not null,
	idCategory int not null,
	price float not null default 0,
	foreign key (idCategory) references FoodCategory(id)
)
go

create table Bill(
	id int identity primary key,
	DateCheckIn date not null default getdate(),
	DateCheckOut date,
	idTable int not null,
	status int not null default 0, --1 or 0
	discount int default 0,
	totalPrice float,
	foreign key (idTable) references TableFood(id)
)
go


create table BillInfo(
	id int identity primary key,
	idBill int not null,
	idFood int not null,
	count int not null default 0,
	foreign key (idBill) references Bill(id),
	foreign key (idFood) references Food(id)
)
go


--Thêm dữ liệu vào bảng Account
insert into Account(UserName,DisplayName,PassWord,Type) values (N'admin',N'Trúc Đinh',N'admin',1)
insert into Account(UserName,DisplayName,PassWord,Type) values (N'nhanvien',N'Nhân viên',N'nhanvien',0)
go


--Thêm dữ liệu vào bảng TableFood
insert into TableFood(name) values (N'Bàn 1')
insert into TableFood(name) values (N'Bàn 2')
insert into TableFood(name) values (N'Bàn 3')
insert into TableFood(name) values (N'Bàn 4')
insert into TableFood(name) values (N'Bàn 5')
insert into TableFood(name) values (N'Bàn 6')
insert into TableFood(name) values (N'Bàn 7')
insert into TableFood(name) values (N'Bàn 8')
insert into TableFood(name) values (N'Bàn 9')
insert into TableFood(name) values (N'Bàn 10')
insert into TableFood(name) values (N'Bàn 11')
insert into TableFood(name) values (N'Bàn 12')
insert into TableFood(name) values (N'Bàn 13')
insert into TableFood(name) values (N'Bàn 14')
insert into TableFood(name) values (N'Bàn 15')
go

--Thêm dữ liệ vào bảng FoodCategory
SET IDENTITY_INSERT [dbo].[FoodCategory] ON
INSERT [dbo].[FoodCategory] ([id], [name]) VALUES (1, N'Trà')
INSERT [dbo].[FoodCategory] ([id], [name]) VALUES (2, N'Cà phê')
INSERT [dbo].[FoodCategory] ([id], [name]) VALUES (3, N'Đá xay')
INSERT [dbo].[FoodCategory] ([id], [name]) VALUES (4, N'Nước ép')
INSERT [dbo].[FoodCategory] ([id], [name]) VALUES (5, N'Trà sửa')
SET IDENTITY_INSERT [dbo].[FoodCategory] OFF
go

--Thêm dữ liệu vào bảng Food
SET IDENTITY_INSERT [dbo].[Food] ON
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (1, N'Trà đào', 1, 30000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (2, N'Trà vải', 1,30000 )
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (3, N'Cà phê đá', 2, 15000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (4, N'Cà  phê sửa', 2, 18000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (5, N'Matcha đá xay', 3, 35000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (6, N'Socola đá xay', 3, 32000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (7, N'Dưa hấu', 4, 12000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (8, N'Cam', 4, 20000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (9, N'Trà sửa truyền thống', 5, 25000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (10, N'Trà sua tran châu đường đen', 5, 27000)
INSERT [dbo].[Food] ([id], [name], [idCategory], [price]) VALUES (15, N'Trà rừng', 1, 30000)
SET IDENTITY_INSERT [dbo].[Food] OFF
go

--Tạo thủ tục
create proc InsertBillInfo @idBill int, @idFood int, @count int
as
begin
	declare @isExistBillInfo int
	declare @foodCount int = 1
	select @isExistBillInfo = id , @foodCount = count FROM BillInfo WHERE idBill = @idBill and idFood = @idFood
	if(@isExistBillInfo > 0)
	begin
		declare @newCount int = @foodCount + @count
		if(@newCount > 0)
			update BillInfo set count = @foodCount + @count where idFood = @idFood
		else
			delete BillInfo where idBill = @idBill and idFood = @idFood
	end
	else
	begin
		insert into BillInfo(idBill,idFood,count) values (@idBill,@idFood,@count)
	end
end
go

create proc SwitchTable @idTable1 int, @idTable2 int 
as
begin
	declare @idFirstBill int
	declare @idSecondBill int

	declare @isFirstTableEmty int = 1
	declare @isSecondTableEmty int = 1

	SELECT @idSecondBill = id FROM Bill WHERE idTable = @idTable2 AND status = 0
	SELECT @idFirstBill = id FROM Bill WHERE idTable = @idTable1 AND status = 0
	if(@idFirstBill is null)
	begin
		insert into Bill(DateCheckIn,DateCheckOut,idTable,status) values (GETDATE(),null,@idTable1,0)
		select @idFirstBill = max(id) from Bill where idTable = @idTable1 and status = 0
	end

	select @isFirstTableEmty = count(*) from BillInfo where idBill = @idFirstBill

	if(@idSecondBill is null)
	begin
		insert into Bill(DateCheckIn,DateCheckOut,idTable,status) values (GETDATE(),null,@idTable2,0)
		select @idSecondBill = max(id) from Bill where idTable = @idTable2 and status = 0
	end

	select @isSecondTableEmty = count(*) from BillInfo where idBill = @idSecondBill

	select id into IDBillIntoTable from BillInfo where idBill = @idSecondBill
	update BillInfo set idBill = @idSecondBill where idBill = @idFirstBill
	update BillInfo set idBill = @idFirstBill where id in (select * from IDBillIntoTable)
	drop table IDBillIntoTable

	if(@isFirstTableEmty = 0)
		update TableFood set status = N'Trống' where id = @idTable2

	if(@isSecondTableEmty = 0)
		update TableFood set status = N'Trống' where id = @idTable1
end
go

create proc GetListBillDate @dateCheckIn date, @dateCheckOut date
as
begin
	select b.name as [Tên bàn], a.totalPrice as [Tổng tiền], a.DateCheckIn as [Ngày vào], a.DateCheckOut as [Ngày ra], a.discount as [Giảm giá (%)] 
	from Bill a, TableFood b 
	where a.idTable = b.id and a.DateCheckIn >= @dateCheckIn and a.DateCheckOut <= @dateCheckOut and a.status = 1
end
go


create proc UpdateAccount @userName nvarchar(100), @displayName nvarchar(100), @passWord nvarchar(100), @newPassWord nvarchar(100)
as
begin
	declare @isRightPass int = 0
	select @isRightPass = count(*) from Account where UserName = @userName and PassWord = @passWord
	if(@isRightPass = 1)
	begin
		if(@newPassWord = null or @newPassWord = '')
		begin
			update Account set DisplayName = @displayName where UserName = @userName
		end
		else
			update Account set DisplayName = @displayName, PassWord = @newPassWord where UserName = @userName
	end
end
go

--Tạo tigger
create trigger UpdateBillInfo
on BillInfo for insert, update
as
begin
	declare @idBill int
	select @idBill = idBill from Inserted
	declare @idTable int
	select @idTable = idTable from Bill where id = @idBill and status = 0
	declare @count int
	select @count = count(*) from BillInfo where idBill = @idBill
	if(@count > 0)
		update TableFood set status = N'Có người' where id = @idTable
	else 
		update TableFood set status = N'Trống' where id = @idTable
end
go

create trigger UpdateBill
on Bill for update
as
begin
	declare @idBill int
	select @idBill = id from Inserted
	declare @idTable int
	select @idTable = idTable from Bill where id = @idBill
	declare @count int = 0
	select @count = count(*) from Bill where idTable = @idTable and status = 0;
	if(@count = 0)
		update TableFood set status = N'Trống' where id = @idTable;
end
go

create trigger DeleteBillInfo
on BillInfo for delete
as
begin
	declare @idBillInfo int
	declare @idBill int
	select @idBillInfo = id, @idBill = Deleted.idBill from Deleted

	declare @idTable int
	select @idTable = idTable from Bill where id = @idBill

	declare @count int = 0
	select @count = count(*) from BillInfo a, Bill b where b.id = a.idBill and b.id = @idBill and b.status = 0

	if(@count = 0)
		update TableFood set status = N'Trống' where id = @idTable
end
go
